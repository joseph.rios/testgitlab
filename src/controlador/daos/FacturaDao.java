/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.daos;

import lista.controlador.Lista;
import modelo.Factura;
import modelo.Producto;

/**
 *
 * @author JosephR
 */
public class FacturaDao extends AdapatadoDao<Factura> {

    private Factura factura;

    public FacturaDao() {
        super(Factura.class);

    }

    public Factura getFactura() {
        if (factura == null) {
            factura = new Factura();
        }

        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public boolean guardar() {
        factura.setId(new Long(listar().tamanio() + 1));
        return guardar(factura);
    }

    public Lista<Factura> buscarCodigo(String dato) {
        Lista<Factura> lista = new Lista<>();
        Lista<Factura> aux = listar();

        for (int i = 0; i < aux.tamanio(); i++) {
            Factura p = aux.consultarDatoPosicion(i);
            Boolean band = p.getNumeroFactura().contains(dato.toLowerCase());
            if (band) {
                lista.insertarNodo(p);
            }
        }
        return lista;
    }

}
