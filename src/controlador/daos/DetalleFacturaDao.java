/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.daos;

import lista.controlador.Lista;
import modelo.DetalleFactura;
import modelo.Factura;
import modelo.Producto;

/**
 *
 * @author JosephR
 */
public class DetalleFacturaDao extends AdapatadoDao<DetalleFactura>{
    private DetalleFactura detalle; 
    

    public DetalleFacturaDao() {
        super(DetalleFactura.class);
        
    }

    public DetalleFactura getDetalle() {
        if(detalle == null)
            detalle = new DetalleFactura();
        
        return detalle;
    }

    public void setDetalle(DetalleFactura detalle) {
        this.detalle = detalle;
    }

    public boolean guardar(){
        detalle.setId(new Long (listar().tamanio()+1));
        return guardar(detalle);
    }

    

    
    
}
