
package controlador.daos;

import lista.controlador.Lista;
import modelo.DetalleFactura;
import modelo.Producto;

/**
 *
 * @author JosephR
 */
public class CtrolDetalleFactura {
    DetalleFactura df = new DetalleFactura();
    Producto producto = new Producto();
    Lista<Producto> lista = new Lista();

    public DetalleFactura getDf() {
        return df;
    }

    public void setDf(DetalleFactura df) {
        this.df = df;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Lista<Producto> getLista() {
        return lista;
    }

    public void setLista(Lista<Producto> lista) {
        this.lista = lista;
    }
    
}
