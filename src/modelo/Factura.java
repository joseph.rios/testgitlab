package modelo;
/**
 *
 * @author JosephR
 */
import java.io.Serializable;
import java.util.Calendar;

public class Factura implements Serializable{
    private Long id;
    private String numeroFactura;
    private Cliente cliente;
    private Boolean facturaAprobada;
    private Calendar fecha;
    private String formaPago;
    private DetalleFactura detalleFactura;

    public DetalleFactura getDetalleFactura() {
        return detalleFactura;
    }

    public void setDetalleFactura(DetalleFactura detalleFactura) {
        this.detalleFactura = detalleFactura;
    }
    

    public String getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(String numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Boolean getFacturaAprobada() {
        return facturaAprobada;
    }

    public void setFacturaAprobada(Boolean facturaAprobada) {
        this.facturaAprobada = facturaAprobada;
    }

    public Calendar getFecha() {
        return fecha;
    }

    public void setFecha(Calendar fecha) {
        this.fecha = fecha;
    }
    

}
