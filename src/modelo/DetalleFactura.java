
package modelo;

import java.io.Serializable;
import lista.controlador.Lista;

/**
 *
 * @author JosephR
 */
public class DetalleFactura implements Serializable {
    private Long id;
    private String numeroFactura;
    private Lista<Producto> productos;
    private Producto producto;

    public Long getId() {
        return id;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(String numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public Lista<Producto> getProductos() {
        return productos;
    }

    public void setProductos(Lista<Producto> productos) {
        this.productos = productos;
    }
    
    

}
