/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.daos.CtrolDetalleFactura;
import controlador.daos.FacturaDao;
import controlador.daos.PersonaDao;
import controlador.daos.ProductoDao;
import java.awt.Color;
import java.text.DecimalFormat;
import java.util.Calendar;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import lista.controlador.Lista;
import vista.modelo.tablaProductoFactura;

/**
 *
 * @author compa
 */
public class Frm_vistaFactura extends javax.swing.JFrame {

    private FacturaDao factura = new FacturaDao();
    private CtrolDetalleFactura detalle = new CtrolDetalleFactura();
    private PersonaDao pD = new PersonaDao();
    private ProductoDao proD = new ProductoDao();
    private tablaProductoFactura mtc = new tablaProductoFactura();
    private Lista aux = new Lista();

    public Frm_vistaFactura() {
        initComponents();
        rellenarComboClientes(cbxClientes);
        rellenarComboProductos(cbxProductos);
        cargarTabla();
    }

    private void cargarTabla() {
        Lista aux = new Lista();
        mtc.setLista(aux);
        tablaProductos.setModel(mtc);
        tablaProductos.updateUI();
        
    }

    private boolean validar() {
        return (txtDireccion.getText().trim().length() > 0
                && txtEmail.getText().trim().length() > 0
                && cbxClientes.getSelectedIndex() != 0
                && txtTotal.getText().trim().length() > 0);
    }

    private void limpiar() {
        txtCantidad.setText("");
        txtCelular.setText("");
        txtDireccion.setText("");
        txtEmail.setText("");
        txtSubTotal.setText("");
        txtSubTotal0.setText("");
        txtSubTotal12.setText("");
        txtTotal.setText("");
        txtvalorTotal.setText("");
        txtValorUnit.setText("");
        cargarTabla();

        cbxClientes.setSelectedItem(0);
        cbxProductos.setSelectedItem(0);
    }

    public void rellenarComboClientes(JComboBox combo) {
        String cliente;
        try {
            combo.removeAllItems();
            combo.addItem("Buscar Clientes");
            for (int i = 0; i < pD.listar().tamanio(); i++) {
                System.out.println("ssss" + pD.listar().consultarDatoPosicion(i).getId());
                cliente = pD.listar().consultarDatoPosicion(i).getApellido() + " " + pD.listar().consultarDatoPosicion(i).getNombre() + " " + pD.listar().consultarDatoPosicion(i).getCedula();
//                    cliente = pD.listar().consultarDatoPosicion(i).getNombre()+ " " +pD.listar().consultarDatoPosicion(i).getCedula();
                combo.addItem(cliente);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al cargar ComboBox" + e);
        }
    }

    public void rellenarComboProductos(JComboBox combo) {
        String cliente;
        try {
            combo.removeAllItems();
            combo.addItem("Buscar Productos");
            for (int i = 0; i < proD.listar().tamanio(); i++) {
                cliente = proD.listar().consultarDatoPosicion(i).getNombre();
//                    cliente = pD.listar().consultarDatoPosicion(i).getNombre()+ " " +pD.listar().consultarDatoPosicion(i).getCedula();
                combo.addItem(cliente);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al cargar ComboBox" + e);
        }
    }

    public void selecionarCliente() {
        try {
            if (cbxClientes.getSelectedIndex() != 0) {
                int id = cbxClientes.getSelectedIndex();
                for (int i = 0; i < cbxClientes.getMaximumRowCount(); i++) {
                    if (pD.listar().consultarDatoPosicion(i).getId() == id) {
                        txtDireccion.setText(pD.listar().consultarDatoPosicion(i).getDireccion());
                        txtEmail.setText(pD.listar().consultarDatoPosicion(i).getCorreo());
                        factura.getFactura().setCliente(pD.listar().consultarDatoPosicion(i));
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    public void selecionarProducto() {
        try {
            if (cbxProductos.getSelectedIndex() != 0) {
                int id = cbxProductos.getSelectedIndex();
                for (int i = 0; i < cbxProductos.getMaximumRowCount(); i++) {
                    if (proD.listar().consultarDatoPosicion(i).getId() == id) {
                        txtValorUnit.setText(proD.listar().consultarDatoPosicion(i).getPrecio().toString());
                        Double vTotal = proD.listar().consultarDatoPosicion(i).getPrecio() * Integer.parseInt(txtCantidad.getText());
                        txtvalorTotal.setText(vTotal.toString());
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    private void llenarTabla() {
        try {
            factura.getFactura().setNumeroFactura("000" + factura.getFactura().getId());
            detalle.getDf().setNumeroFactura(detalle.getDf().getNumeroFactura());
            
            if (cbxProductos.getSelectedIndex() != 0) {
                int id = cbxProductos.getSelectedIndex();
                for (int i = 0; i < cbxProductos.getMaximumRowCount(); i++) {
                    if (proD.listar().consultarDatoPosicion(i).getId() == id) {
                        detalle.setProducto(proD.listar().consultarDatoPosicion(i));
                        int unidades = Integer.parseInt(txtCantidad.getText());
                        detalle.getProducto().setUnidades(unidades);
                        detalle.getProducto().setPrecio(proD.listar().consultarDatoPosicion(i).getPrecio() * unidades);
                        detalle.getProducto().setPrecioIva(proD.listar().consultarDatoPosicion(i).getIva() / 100 * detalle.getProducto().getPrecio() + detalle.getProducto().getPrecio());
                        detalle.getLista().insertarNodo(detalle.getProducto());
//                        aux.insertarNodo(detalle.getLista());
                        
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("error" + e);
        }
//        detalle.getDf().setProductos(aux);
        mtc.setLista(detalle.getLista());
        detalle.getDf().setProductos(mtc.getLista());
        tablaProductos.setModel(mtc);
        tablaProductos.updateUI();

        llenarPrecios();
    }

    public void llenarPrecios() {
        Double SubTotal = 0.0;
        Double IvaTotal = 0.0;
        Double Total = 0.0;
        DecimalFormat df = new DecimalFormat("###.##");

        for (int i = 0; i < detalle.getLista().tamanio(); i++) {
            SubTotal += detalle.getLista().consultarDatoPosicion(i).getPrecio();
            IvaTotal += detalle.getLista().consultarDatoPosicion(i).getPrecioIva() - detalle.getLista().consultarDatoPosicion(i).getPrecio();
            Total += detalle.getLista().consultarDatoPosicion(i).getPrecioIva();
            txtSubTotal.setText(df.format(SubTotal));
            txtSubTotal12.setText(df.format(IvaTotal));
            txtTotal.setText(df.format(Total));
        }

    }

    public String tipoPago() {
        String tipoDePago = "";
        if (radioEfectivo.isSelected()) {
            tipoDePago = "Efectivo";
        } else if (radioCredito.isSelected()) {
            tipoDePago = "Crédito/Débito";
        } else if (radioDepositoTransferencia.isSelected()) {
            tipoDePago = "Depósito/Transferencia";
        } else if (radioOtros.isSelected()) {
            tipoDePago = "Otro";
        }
        return tipoDePago;
    }

    private void guardar(Boolean aprobada) {
        if (validar()) {
            factura.getFactura().setFacturaAprobada(aprobada);
            factura.getFactura().setDetalleFactura(detalle.getDf());
            System.out.println("--"+factura.getFactura().getDetalleFactura().getProductos().consultarDatoPosicion(0).getNombre());;
            Calendar fecha = Calendar.getInstance();
            factura.getFactura().setFecha(fecha);
            factura.getFactura().setFormaPago(tipoPago());

            if (factura.guardar()) {
                JOptionPane.showMessageDialog(null, "Guardado con exito", "Ok", JOptionPane.INFORMATION_MESSAGE);
                limpiar();
            } else {
                JOptionPane.showMessageDialog(null, "No se puede guardar", "Error", JOptionPane.ERROR_MESSAGE);

            }
        } else {
            JOptionPane.showMessageDialog(null, "No se puede guardar", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel9 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable4 = new javax.swing.JTable();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        buttonGroup4 = new javax.swing.ButtonGroup();
        buttonGroup5 = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        panel_cerrarSesion = new javax.swing.JPanel();
        btn_cerrarSesion = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        btn_consulClientes = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        btn_consulProductos = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        btn_agregarClientes = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        btn_agregarProductos = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaProductos = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTable5 = new javax.swing.JTable();
        jLabel18 = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTable6 = new javax.swing.JTable();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        txtCantidad = new javax.swing.JTextField();
        txtValorUnit = new javax.swing.JTextField();
        txtvalorTotal = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        cbxProductos = new javax.swing.JComboBox<>();
        agregarProd = new javax.swing.JButton();
        jLabel25 = new javax.swing.JLabel();
        txtSubTotal0 = new javax.swing.JTextField();
        txtSubTotal12 = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        txtSubTotal = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        radioOtros = new javax.swing.JRadioButton();
        radioEfectivo = new javax.swing.JRadioButton();
        radioCredito = new javax.swing.JRadioButton();
        radioDepositoTransferencia = new javax.swing.JRadioButton();
        Facturar = new javax.swing.JButton();
        Facturar1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtCelular = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtEmail = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtDireccion = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        cbxClientes = new javax.swing.JComboBox<>();

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel9.setLayout(null);

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(jTable3);

        jPanel9.add(jScrollPane3);
        jScrollPane3.setBounds(10, 150, 570, 80);

        jLabel13.setText("Detalle de la factura (Moneda dólares americanos $$)");
        jPanel9.add(jLabel13);
        jLabel13.setBounds(10, 10, 270, 14);

        jLabel14.setText("Datos del Comprador");
        jPanel9.add(jLabel14);
        jLabel14.setBounds(10, 110, 101, 14);

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel10.setLayout(null);

        jTable4.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(jTable4);

        jPanel10.add(jScrollPane4);
        jScrollPane4.setBounds(10, 150, 570, 80);

        jLabel15.setText("Detalle de la factura (Moneda dólares americanos $$)");
        jPanel10.add(jLabel15);
        jLabel15.setBounds(10, 10, 270, 14);

        jLabel16.setText("Datos del Comprador");
        jPanel10.add(jLabel16);
        jLabel16.setBounds(10, 110, 101, 14);

        jPanel9.add(jPanel10);
        jPanel10.setBounds(0, 0, 0, 0);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(51, 0, 102));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        panel_cerrarSesion.setBackground(new java.awt.Color(51, 0, 102));

        btn_cerrarSesion.setBackground(new java.awt.Color(51, 0, 102));
        btn_cerrarSesion.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        btn_cerrarSesion.setForeground(new java.awt.Color(255, 255, 255));
        btn_cerrarSesion.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btn_cerrarSesion.setText("Cerrar Sesión");
        btn_cerrarSesion.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_cerrarSesion.setName(""); // NOI18N
        btn_cerrarSesion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_cerrarSesionMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_cerrarSesionMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_cerrarSesionMouseExited(evt);
            }
        });

        javax.swing.GroupLayout panel_cerrarSesionLayout = new javax.swing.GroupLayout(panel_cerrarSesion);
        panel_cerrarSesion.setLayout(panel_cerrarSesionLayout);
        panel_cerrarSesionLayout.setHorizontalGroup(
            panel_cerrarSesionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_cerrarSesion, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
        );
        panel_cerrarSesionLayout.setVerticalGroup(
            panel_cerrarSesionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_cerrarSesion, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(panel_cerrarSesion, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 440, 170, 30));

        jPanel4.setBackground(new java.awt.Color(51, 0, 102));

        btn_consulClientes.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        btn_consulClientes.setForeground(new java.awt.Color(255, 255, 255));
        btn_consulClientes.setText("          Consultar Clientes          >");
        btn_consulClientes.setToolTipText("");
        btn_consulClientes.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_consulClientes.setName(""); // NOI18N
        btn_consulClientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_consulClientesMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_consulClientes, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_consulClientes, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 120, 170, -1));

        jPanel5.setBackground(new java.awt.Color(51, 0, 102));

        btn_consulProductos.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        btn_consulProductos.setForeground(new java.awt.Color(255, 255, 255));
        btn_consulProductos.setText("          Consultar Productos       >");
        btn_consulProductos.setToolTipText("");
        btn_consulProductos.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_consulProductos.setName(""); // NOI18N
        btn_consulProductos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_consulProductosMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_consulProductos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_consulProductos, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 150, 170, -1));

        jPanel6.setBackground(new java.awt.Color(51, 0, 102));

        btn_agregarClientes.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        btn_agregarClientes.setForeground(new java.awt.Color(255, 255, 255));
        btn_agregarClientes.setText("          + Agregar Clientes          >");
        btn_agregarClientes.setToolTipText("");
        btn_agregarClientes.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_agregarClientes.setName(""); // NOI18N
        btn_agregarClientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_agregarClientesMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_agregarClientes, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_agregarClientes, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 180, 170, -1));

        jPanel7.setBackground(new java.awt.Color(51, 0, 102));

        btn_agregarProductos.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        btn_agregarProductos.setForeground(new java.awt.Color(255, 255, 255));
        btn_agregarProductos.setText("          + Agregar Productos       >");
        btn_agregarProductos.setToolTipText("");
        btn_agregarProductos.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btn_agregarProductos.setName(""); // NOI18N
        btn_agregarProductos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btn_agregarProductosMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_agregarProductos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btn_agregarProductos, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 210, 170, -1));

        jPanel2.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 170, 490));

        jLabel1.setFont(new java.awt.Font("Roboto", 1, 18)); // NOI18N
        jLabel1.setText("FACTURAR");
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 10, -1, -1));
        jPanel2.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 30, 580, 10));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setLayout(null);

        tablaProductos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tablaProductos);

        jPanel3.add(jScrollPane1);
        jScrollPane1.setBounds(10, 100, 570, 90);

        jLabel6.setText("Detalle de la factura (Moneda dólares americanos $)");
        jPanel3.add(jLabel6);
        jLabel6.setBounds(10, 10, 270, 14);

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel8.setLayout(null);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(jTable2);

        jPanel8.add(jScrollPane2);
        jScrollPane2.setBounds(10, 150, 570, 80);

        jLabel11.setText("Detalle de la factura (Moneda dólares americanos $$)");
        jPanel8.add(jLabel11);
        jLabel11.setBounds(10, 10, 270, 14);

        jLabel12.setText("Datos del Comprador");
        jPanel8.add(jLabel12);
        jLabel12.setBounds(10, 110, 101, 14);

        jPanel3.add(jPanel8);
        jPanel8.setBounds(0, 0, 0, 0);

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));
        jPanel11.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel11.setLayout(null);

        jTable5.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane5.setViewportView(jTable5);

        jPanel11.add(jScrollPane5);
        jScrollPane5.setBounds(10, 150, 570, 80);

        jLabel18.setText("Datos del Comprador");
        jPanel11.add(jLabel18);
        jLabel18.setBounds(10, 110, 101, 14);

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));
        jPanel12.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel12.setLayout(null);

        jTable6.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane6.setViewportView(jTable6);

        jPanel12.add(jScrollPane6);
        jScrollPane6.setBounds(10, 150, 570, 80);

        jLabel19.setText("Detalle de la factura (Moneda dólares americanos $$)");
        jPanel12.add(jLabel19);
        jLabel19.setBounds(10, 10, 270, 14);

        jLabel20.setText("Datos del Comprador");
        jPanel12.add(jLabel20);
        jLabel20.setBounds(10, 110, 101, 14);

        jPanel11.add(jPanel12);
        jPanel12.setBounds(0, 0, 0, 0);

        jLabel21.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("Cant.");
        jPanel11.add(jLabel21);
        jLabel21.setBounds(10, 10, 60, 15);

        jLabel22.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel22.setText("Descripción");
        jPanel11.add(jLabel22);
        jLabel22.setBounds(190, 10, 60, 15);

        jLabel23.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel23.setText("V.Unit.");
        jPanel11.add(jLabel23);
        jLabel23.setBounds(380, 10, 50, 15);
        jPanel11.add(txtCantidad);
        txtCantidad.setBounds(10, 30, 60, 30);
        jPanel11.add(txtValorUnit);
        txtValorUnit.setBounds(380, 30, 50, 30);
        jPanel11.add(txtvalorTotal);
        txtvalorTotal.setBounds(440, 30, 50, 30);

        jLabel24.setFont(new java.awt.Font("Roboto", 0, 11)); // NOI18N
        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel24.setText("V.Total.");
        jPanel11.add(jLabel24);
        jLabel24.setBounds(440, 10, 50, 15);

        cbxProductos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbxProductos.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbxProductosItemStateChanged(evt);
            }
        });
        jPanel11.add(cbxProductos);
        cbxProductos.setBounds(80, 30, 290, 30);

        agregarProd.setText("OK");
        agregarProd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregarProdActionPerformed(evt);
            }
        });
        jPanel11.add(agregarProd);
        agregarProd.setBounds(500, 30, 60, 30);

        jPanel3.add(jPanel11);
        jPanel11.setBounds(10, 30, 570, 70);

        jLabel25.setFont(new java.awt.Font("Roboto", 0, 10)); // NOI18N
        jLabel25.setText("Desarrollado por G2UNL - Derechos Reservados");
        jPanel3.add(jLabel25);
        jLabel25.setBounds(10, 290, 230, 14);

        txtSubTotal0.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        txtSubTotal0.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtSubTotal0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSubTotal0ActionPerformed(evt);
            }
        });
        jPanel3.add(txtSubTotal0);
        txtSubTotal0.setBounds(520, 200, 60, 19);

        txtSubTotal12.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        txtSubTotal12.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtSubTotal12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSubTotal12ActionPerformed(evt);
            }
        });
        jPanel3.add(txtSubTotal12);
        txtSubTotal12.setBounds(520, 220, 60, 19);

        jLabel26.setFont(new java.awt.Font("Roboto", 0, 10)); // NOI18N
        jLabel26.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel26.setText("SUB TOTAL 12% IVA $");
        jPanel3.add(jLabel26);
        jLabel26.setBounds(380, 220, 130, 14);

        jLabel27.setFont(new java.awt.Font("Roboto", 0, 10)); // NOI18N
        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel27.setText("SUB TOTAL $");
        jPanel3.add(jLabel27);
        jLabel27.setBounds(380, 240, 130, 14);

        txtSubTotal.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        txtSubTotal.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtSubTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSubTotalActionPerformed(evt);
            }
        });
        jPanel3.add(txtSubTotal);
        txtSubTotal.setBounds(520, 240, 60, 19);

        jLabel28.setFont(new java.awt.Font("Roboto", 1, 10)); // NOI18N
        jLabel28.setText("FORMA DE PAGO");
        jPanel3.add(jLabel28);
        jLabel28.setBounds(10, 210, 130, 14);

        txtTotal.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        txtTotal.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        txtTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalActionPerformed(evt);
            }
        });
        jPanel3.add(txtTotal);
        txtTotal.setBounds(520, 260, 60, 19);

        jLabel29.setFont(new java.awt.Font("Roboto", 0, 10)); // NOI18N
        jLabel29.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel29.setText("SUB TOTAL 0% IVA $");
        jPanel3.add(jLabel29);
        jLabel29.setBounds(380, 200, 130, 14);

        jLabel30.setFont(new java.awt.Font("Roboto", 1, 10)); // NOI18N
        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel30.setText("VALOR TOTAL $");
        jPanel3.add(jLabel30);
        jLabel30.setBounds(380, 260, 130, 14);

        radioOtros.setFont(new java.awt.Font("Roboto", 0, 10)); // NOI18N
        radioOtros.setText("Otros");
        jPanel3.add(radioOtros);
        radioOtros.setBounds(330, 230, 60, 23);

        radioEfectivo.setFont(new java.awt.Font("Roboto", 0, 10)); // NOI18N
        radioEfectivo.setText("Efectivo");
        radioEfectivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioEfectivoActionPerformed(evt);
            }
        });
        jPanel3.add(radioEfectivo);
        radioEfectivo.setBounds(10, 230, 70, 23);

        radioCredito.setFont(new java.awt.Font("Roboto", 0, 10)); // NOI18N
        radioCredito.setText("T.Crédito/Débito");
        jPanel3.add(radioCredito);
        radioCredito.setBounds(80, 230, 110, 23);

        radioDepositoTransferencia.setFont(new java.awt.Font("Roboto", 0, 10)); // NOI18N
        radioDepositoTransferencia.setText("Depósito/Transferencia");
        jPanel3.add(radioDepositoTransferencia);
        radioDepositoTransferencia.setBounds(190, 230, 140, 23);

        Facturar.setText("Proformar");
        Facturar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FacturarActionPerformed(evt);
            }
        });
        jPanel3.add(Facturar);
        Facturar.setBounds(350, 280, 90, 23);

        Facturar1.setText("Facturar");
        Facturar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Facturar1ActionPerformed(evt);
            }
        });
        jPanel3.add(Facturar1);
        Facturar1.setBounds(250, 280, 90, 23);

        jPanel2.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 170, 590, 310));

        jLabel2.setFont(new java.awt.Font("Roboto", 0, 10)); // NOI18N
        jLabel2.setText("CLIENTE:");
        jPanel2.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 60, -1, -1));

        jLabel4.setFont(new java.awt.Font("Roboto", 0, 10)); // NOI18N
        jLabel4.setText("TELEFONO DOMICILIO:");
        jPanel2.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 60, -1, -1));

        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });
        jPanel2.add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 80, 120, -1));

        jLabel5.setFont(new java.awt.Font("Roboto", 0, 10)); // NOI18N
        jLabel5.setText("TELEFONO MOVIL:");
        jPanel2.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 60, -1, -1));
        jPanel2.add(txtCelular, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 80, 100, -1));

        jLabel7.setFont(new java.awt.Font("Roboto", 0, 10)); // NOI18N
        jLabel7.setText("EMAIL:");
        jPanel2.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 110, -1, -1));

        txtEmail.setToolTipText("");
        jPanel2.add(txtEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 130, 200, -1));
        jPanel2.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 160, 590, 10));

        jLabel9.setFont(new java.awt.Font("Roboto", 0, 10)); // NOI18N
        jLabel9.setText("DIRECCIÓN COMPRADOR:");
        jPanel2.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 110, -1, -1));

        jLabel10.setText("Datos del Comprador");
        jPanel2.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 40, -1, -1));
        jPanel2.add(txtDireccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 130, 370, -1));
        jPanel2.add(jTextField3, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 80, 60, -1));

        jLabel8.setFont(new java.awt.Font("Roboto", 0, 10)); // NOI18N
        jLabel8.setText("GUÍA REM.:");
        jPanel2.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 60, -1, -1));

        cbxClientes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbxClientes.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbxClientesItemStateChanged(evt);
            }
        });
        jPanel2.add(cbxClientes, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 80, 270, -1));

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 780, 490));

        setSize(new java.awt.Dimension(797, 528));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btn_consulClientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_consulClientesMouseClicked
        // TODO add your handling code here:
        Frm_Clientes nuevoConsulCliente = new Frm_Clientes();
        nuevoConsulCliente.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btn_consulClientesMouseClicked

    private void btn_consulProductosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_consulProductosMouseClicked
        // TODO add your handling code here:
        Frm_Productos nuevoConsulProductos = new Frm_Productos();
        nuevoConsulProductos.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btn_consulProductosMouseClicked

    private void btn_agregarClientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_agregarClientesMouseClicked
        // TODO add your handling code here:
        Frm_agregarClientes nuevoAgregarClientes = new Frm_agregarClientes();
        nuevoAgregarClientes.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btn_agregarClientesMouseClicked

    private void btn_agregarProductosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_agregarProductosMouseClicked
        // TODO add your handling code here:
        Frm_agregarProductos nuevoAgregarProductos = new Frm_agregarProductos();
        nuevoAgregarProductos.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btn_agregarProductosMouseClicked

    private void btn_cerrarSesionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_cerrarSesionMouseClicked
        // TODO add your handling code here:
        Frm_vistaLogin nuevoLogin = new Frm_vistaLogin();
        nuevoLogin.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btn_cerrarSesionMouseClicked

    private void btn_cerrarSesionMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_cerrarSesionMouseEntered
        // TODO add your handling code here:
        panel_cerrarSesion.setBackground(Color.white);

    }//GEN-LAST:event_btn_cerrarSesionMouseEntered

    private void btn_cerrarSesionMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_cerrarSesionMouseExited
        // TODO add your handling code here:
        panel_cerrarSesion.setBackground(Color.blue);

    }//GEN-LAST:event_btn_cerrarSesionMouseExited

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void txtSubTotal0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSubTotal0ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSubTotal0ActionPerformed

    private void txtSubTotal12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSubTotal12ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSubTotal12ActionPerformed

    private void txtSubTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSubTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSubTotalActionPerformed

    private void txtTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalActionPerformed

    private void radioEfectivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioEfectivoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_radioEfectivoActionPerformed

    private void cbxClientesItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbxClientesItemStateChanged
        selecionarCliente();
    }//GEN-LAST:event_cbxClientesItemStateChanged

    private void cbxProductosItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbxProductosItemStateChanged
        selecionarProducto();
    }//GEN-LAST:event_cbxProductosItemStateChanged

    private void agregarProdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agregarProdActionPerformed
        llenarTabla();
    }//GEN-LAST:event_agregarProdActionPerformed

    private void Facturar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Facturar1ActionPerformed
        guardar(true);
    }//GEN-LAST:event_Facturar1ActionPerformed

    private void FacturarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FacturarActionPerformed
        guardar(false);
    }//GEN-LAST:event_FacturarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Frm_vistaFactura.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Frm_vistaFactura.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Frm_vistaFactura.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Frm_vistaFactura.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Frm_vistaFactura().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Facturar;
    private javax.swing.JButton Facturar1;
    private javax.swing.JButton agregarProd;
    private javax.swing.JLabel btn_agregarClientes;
    private javax.swing.JLabel btn_agregarProductos;
    private javax.swing.JLabel btn_cerrarSesion;
    private javax.swing.JLabel btn_consulClientes;
    private javax.swing.JLabel btn_consulProductos;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.ButtonGroup buttonGroup4;
    private javax.swing.ButtonGroup buttonGroup5;
    private javax.swing.JComboBox<String> cbxClientes;
    private javax.swing.JComboBox<String> cbxProductos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    private javax.swing.JTable jTable4;
    private javax.swing.JTable jTable5;
    private javax.swing.JTable jTable6;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JPanel panel_cerrarSesion;
    private javax.swing.JRadioButton radioCredito;
    private javax.swing.JRadioButton radioDepositoTransferencia;
    private javax.swing.JRadioButton radioEfectivo;
    private javax.swing.JRadioButton radioOtros;
    private javax.swing.JTable tablaProductos;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtCelular;
    private javax.swing.JTextField txtDireccion;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtSubTotal;
    private javax.swing.JTextField txtSubTotal0;
    private javax.swing.JTextField txtSubTotal12;
    private javax.swing.JTextField txtTotal;
    private javax.swing.JTextField txtValorUnit;
    private javax.swing.JTextField txtvalorTotal;
    // End of variables declaration//GEN-END:variables
}
